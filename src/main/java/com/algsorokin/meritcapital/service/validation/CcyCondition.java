package com.algsorokin.meritcapital.service.validation;

import com.algsorokin.meritcapital.constants.ErrorMessages;
import com.algsorokin.meritcapital.entity.Trade;
import com.algsorokin.meritcapital.utils.CcyIsoOrgParser;
import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * Class for validation currency codes from currency fields of trades.
 */
public class CcyCondition implements ValidationCondition {
    private final Logger logger = Logger.getLogger(CcyCondition.class);
    /**
     * On the page https://docs.oracle.com/javase/8/docs/api/java/util/Currency.html i found link to
     * https://www.iso.org/iso-4217-currency-codes.html and there i found file
     * https://www.currency-iso.org/dam/downloads/lists/list_one.xml
     * <p>
     * I parse this file and found all currencies not represented in Java 8 Currency class by executing this code:
     * Set<String> res = new HashSet<>();
     * for (String s: allCodes){
     * try {
     * Currency curr = Currency.getInstance(s);
     * } catch (Exception e){
     * res.add(s);
     * }
     * }
     * Currencies not represented in Java 8 Currency class are in additionalCurr
     * <p>
     * <p>
     * <p>
     * <p>
     * CcyCondition class has 2 ways to validate CcyCodes:
     * 1. If parsing of file (https://www.currency-iso.org/dam/downloads/lists/list_one.xml) is successful
     * ccyCodesFromIsoOrg is used for validation
     * 2. If not, simple checking of availability with {@link Currency} and additionalCurr
     */
    private final Set<String> additionalCurr = Collections.unmodifiableSet(
            /**
             * Store all currencies not represented in Java 8 Currency
             */
            new HashSet<>(Arrays.asList("MRU", "UYW", "VES", "STN")));

    private boolean useIsoOrgDownloadedCurr = false;
    private final Set<String> ccyCodesFromIsoOrg;

    public CcyCondition() {
        CcyIsoOrgParser ccyIsoOrgParser = new CcyIsoOrgParser();
        ccyCodesFromIsoOrg = Collections.unmodifiableSet(ccyIsoOrgParser.parseCcyFromIsoOrg());
        useIsoOrgDownloadedCurr = !CollectionUtils.isEmpty(ccyCodesFromIsoOrg);
    }

    /**
     * Method for validation currency codes (iso-4217) from CcyPair, payCcy, premiumCcy
     *
     * @param trade - trade to validate
     * @return empty Optional, if condition satisfied
     * Optional of Set of errors, if the checks fail
     * <p>
     * Conditions:
     * 1. CcyPair must be present and valid (6 letters)
     * 2. It is assumed that ccy codes must be in upper case
     * 2. CcyPair divides into 2 parts - baseCcy and quoteCcy and codes are checking in accordance with (iso-4217)
     * 3. payCcy and premiumCcy (if presented) are checking in accordance with (iso-4217)
     */
    @Override
    public Optional<Set<String>> validate(Trade trade) {
        Set<String> result = new HashSet<>();
        Map<String, String> codesToVslidate = new HashMap<>();

        if (StringUtils.isEmpty(trade.getCcyPair())) {
            logger.error(String.format("Trade id: %d. " + ErrorMessages.EMPTY_CCY_PAIR, trade.getId()));
            result.add(ErrorMessages.EMPTY_CCY_PAIR);
        } else if (trade.getCcyPair().length() != 6) {
            logger.error(String.format("Trade id: %d. " + ErrorMessages.NOT_VALID_CCY_PAIR, trade.getId()));
            result.add(ErrorMessages.NOT_VALID_CCY_PAIR);
        } else {
            String baseCcy = trade.getCcyPair().substring(0, 3);
            String quoteCcy = trade.getCcyPair().substring(3);
            codesToVslidate.put("baseCcy", baseCcy);
            codesToVslidate.put("quoteCcy", quoteCcy);
        }

        if (!StringUtils.isEmpty(trade.getPayCcy())) {
            codesToVslidate.put("payCcy", trade.getPayCcy());
        }
        if (!StringUtils.isEmpty(trade.getPremiumCcy())) {
            codesToVslidate.put("premiumCcy", trade.getPremiumCcy());
        }
        codesToVslidate.forEach((ccy, code) -> {
            if (!isCcyCodeValid(code)) {
                String mess = String.format("Currency code \"%s\" in field \"%s\" is not not compliant with ISO " +
                        "4217", code, ccy);
                logger.error(mess);
                result.add(mess);
            }

        });

        return CollectionUtils.isEmpty(result) ? Optional.empty() : Optional.of(result);
    }

    /**
     * Checks currency code validity
     *
     * @param ccyCode cheking currency code
     * @return true if valid currency code
     * false if not valid
     */
    private boolean isCcyCodeValid(String ccyCode) {
        if (useIsoOrgDownloadedCurr) {
            return ccyCodesFromIsoOrg.contains(ccyCode);
        } else {
            try {
                Currency.getInstance(ccyCode);
            } catch (Exception e) {
                if (!additionalCurr.contains(ccyCode)) {
                    return false;
                }
            }
        }
        return true;
    }
}
