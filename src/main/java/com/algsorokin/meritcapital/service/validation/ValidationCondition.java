package com.algsorokin.meritcapital.service.validation;

import com.algsorokin.meritcapital.entity.Trade;

import java.util.Optional;
import java.util.Set;

/**
 * Interface for all Validation conditions. Allows service to be flexible to extend validation logic
 * in terms of new business validation rules
 */
@FunctionalInterface
public interface ValidationCondition {
    /**
     * Validate conditions
     *
     * @param trade - trade to validate
     * @return empty Optional, if condition satisfied
     * Optional of Set of errors, if the checks fail
     */
    Optional<Set<String>> validate(final Trade trade);
}
