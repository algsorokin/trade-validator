package com.algsorokin.meritcapital.service.validation;

import java.util.ArrayList;

/**
 * Main validator class for consolidation all conditions.
 * Helps service to be flexible to extend the validation logic
 */
public class MainValidator {
    /**
     * All validation conditions. Supports adding of new conditions
     */
    private static final ArrayList<ValidationCondition> CONDITIONS = new ArrayList<>();

    static {
        CONDITIONS.add(new LegalEntityCondition());
        CONDITIONS.add(new CounterpartyCondition());
        CONDITIONS.add(new TradeTypeCondition());
        CONDITIONS.add(new CcyCondition());
        CONDITIONS.add(new DateCondition("holidays.json"));
    }

    /**
     * Get method for all conditions
     *
     * @return all conditions initialised in static block
     */
    public static ArrayList<ValidationCondition> getConditions() {
        return CONDITIONS;
    }
}
