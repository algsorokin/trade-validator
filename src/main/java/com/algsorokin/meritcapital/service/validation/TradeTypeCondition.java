package com.algsorokin.meritcapital.service.validation;

import com.algsorokin.meritcapital.constants.ErrorMessages;
import com.algsorokin.meritcapital.entity.Trade;
import com.algsorokin.meritcapital.enums.OptionStyle;
import com.algsorokin.meritcapital.enums.TradeType;
import com.algsorokin.meritcapital.utils.TradeTypeUtil;
import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Class for validation TradeTypes
 */
public class TradeTypeCondition implements ValidationCondition {
    private final Logger logger = Logger.getLogger(TradeTypeCondition.class);

    /**
     * Validates type field value of current trade.
     * Condition satisfied, if type contained in enum {@link TradeType}
     * <p>
     * ATTENTION! Validation is not case sensitive.
     * <p>
     * <p>
     * <p>
     * Conditions:
     * 1. if type is Empty the corresponding error text will be returned
     * 2. if type == VanillaOption, we make additional check - validated field is style
     * 3. if style is Empty the corresponding error text will be returned
     * 4. Condition satisfied, if style contained in enum {@link OptionStyle)
     *
     * @param trade - trade to validate
     * @return empty Optional, if condition satisfied
     * Optional of Set of errors, if the checks fail
     */
    @Override
    public Optional<Set<String>> validate(Trade trade) {
        Set<String> result = new HashSet<>();
        if (StringUtils.isEmpty(trade.getType())) {
            logger.error(String.format("Trade id: %d. " + ErrorMessages.EMPTY_TRADE_TYPE, trade.getId()));
            result.add(ErrorMessages.EMPTY_TRADE_TYPE);
        } else {

            TradeType tradeType = TradeTypeUtil.foundTradeType(trade.getType());

            if (tradeType == null) {
                String mess = String.format("Trade type \"%s\" is forbidden.", trade.getType());
                logger.error(mess);
                result.add(mess);
            } else {
                if (tradeType.equals(TradeType.VANILLAOPTION)) {
                    if (StringUtils.isEmpty(trade.getStyle())) {
                        logger.error(String.format("Trade id: %d. " + ErrorMessages.EMPTY_OPTION_STYLE, trade.getId()));
                        result.add(ErrorMessages.EMPTY_OPTION_STYLE);
                    } else {

                        OptionStyle optionStyle = Arrays.stream(OptionStyle.values()).filter(e -> e.getStyleName().
                                equalsIgnoreCase(trade.getStyle())).findAny().orElse(null);
                        if (optionStyle == null) {
                            String mess = String.format("Option style \"%s\" is forbidden.", trade.getStyle());
                            logger.error(mess);
                            result.add(mess);
                        }
                    }
                }
            }
        }
        return CollectionUtils.isEmpty(result) ? Optional.empty() : Optional.of(result);
    }
}
