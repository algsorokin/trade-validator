package com.algsorokin.meritcapital.service.validation;

import com.algsorokin.meritcapital.constants.ErrorMessages;
import com.algsorokin.meritcapital.entity.Trade;
import com.algsorokin.meritcapital.enums.OptionStyle;
import com.algsorokin.meritcapital.enums.TradeType;
import com.algsorokin.meritcapital.utils.HolidayParser;
import com.algsorokin.meritcapital.utils.TradeTypeUtil;
import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.*;

/**
 * Class for validation dates of trade
 */
public class DateCondition implements ValidationCondition {
    private final Logger logger = Logger.getLogger(DateCondition.class);
    /**
     * Set of holidays parsed from resources file
     * ATTENTION! WHERE ARE RUSSIAN HOLIDAYS IN FILE NOW!
     */
    private final Set<LocalDate> holidays;
    /**
     * Set of Option trade type products (now only Vanilla) for checking options dates
     */
    private final Set<TradeType> optionsCheckProducts;

    /**
     * Map of trade type products with value date (now Spot and Forward)
     * <p>
     * key - trade type {@link TradeType}
     * <p>
     * value - working days from trade date to value date:
     * <p>
     * TWO BUSINESS DAYS FOR SPOT - https://en.wikipedia.org/wiki/Spot_contract:
     * In finance, a spot contract, spot transaction, or simply spot, is a contract of buying or selling a commodity,
     * security or currency for immediate settlement (payment and delivery) on the spot date, which is normally
     * two business days after the trade date.
     * <p>
     * I CHOOSE 7 BUSINESS DAYS FOR FORWARD, BECAUSE WHERE IS NO INFORMATION ABOUT EXACT TERM -
     * https://en.wikipedia.org/wiki/Forward_contract:
     * In finance, a forward contract or simply a forward is a non-standardized contract between two parties to buy
     * or to sell an asset at a specified future time at a price agreed upon today, making it a type of derivative
     * instrument.
     *
     */
    private final Map<TradeType, Integer> valueDateCheckProducts;

    public DateCondition(String path) {
        HolidayParser hp = new HolidayParser();
        holidays = hp.parseHolidays(path);

        /**
         * This collections can be extended with new product types
         */
        this.optionsCheckProducts = new HashSet<>(Arrays.asList(TradeType.VANILLAOPTION));
        this.valueDateCheckProducts = new HashMap<TradeType, Integer>() {{
            put(TradeType.SPOT, 2);
            put(TradeType.FORWARD, 7);
        }};
    }

    /**
     * Main method for validate all dates of trade. Detailed description of all conditions are in javaDoc of
     * other validation methods
     *
     * @param trade - trade to validate
     * @return empty Optional, if condition satisfied
     * Optional of Set of errors, if the checks fail
     */
    @Override
    public Optional<Set<String>> validate(Trade trade) {
        Set<String> result = new HashSet<>();

        if (trade.getType() == null) {
            logger.error(String.format("Trade id: %d. " + ErrorMessages.EMPTY_TRADE_TYPE, trade.getId()));
            result.add(ErrorMessages.EMPTY_TRADE_TYPE);
        } else {
            TradeType tradeType = TradeTypeUtil.foundTradeType(trade.getType());
            if (tradeType == null) {
                String mess = String.format("Trade type \"%s\" is forbidden.", trade.getType());
                logger.error(mess);
                result.add(mess);
            } else {
                if (valueDateCheckProducts.containsKey(tradeType)) {
                    validateValueDate(trade, valueDateCheckProducts.get(tradeType), result);
                } else if (optionsCheckProducts.contains(tradeType)) {
                    validateOptionsDates(trade, result);
                }
            }
        }

        return CollectionUtils.isEmpty(result) ? Optional.empty() : Optional.of(result);
    }

    /**
     * Method for validation products with valueDate (now Spot and Forward)
     * <p>
     * Conditions:
     * 1. tradeDate must be present
     * 2. valueDate must be present if type is spot or forward
     * 3. valueDate must be after trade date
     * 4. valueDate must be working day
     * 5. valueDate must be N working days after trade date (N depends on product type)
     *
     * @param trade  trade to validate
     * @param result Set of errors
     */
    private void validateValueDate(Trade trade, Integer plusDays, Set<String> result) {
        LocalDate tradeDate = trade.getTradeDate();
        LocalDate valueDate = trade.getValueDate();

        boolean toDoCheck = true;
        if (tradeDate == null) {
            logger.error(String.format("Trade id: %d. " + ErrorMessages.EMPTY_TRADE_DATE, trade.getId()));
            result.add(ErrorMessages.EMPTY_TRADE_DATE);
            toDoCheck = false;
        }
        if (valueDate == null) {
            logger.error(String.format("Trade id: %d. " + ErrorMessages.EMPTY_VALUE_DATE, trade.getId()));
            result.add(ErrorMessages.EMPTY_VALUE_DATE);
            toDoCheck = false;
        } else {
            if (!isWorkingDay(valueDate)) {
                logger.error(String.format("Trade id: %d. " + ErrorMessages.NOT_WORKING_VALUE_DATE, trade.getId()));
                result.add(ErrorMessages.NOT_WORKING_VALUE_DATE);
            }
        }

        if (toDoCheck) {
            if (valueDate.isBefore(tradeDate)) {
                logger.error(String.format("Trade id: %d. " + ErrorMessages.VALUE_DATE_BEFORE_TRADE_DATE, trade.getId()));
                result.add(ErrorMessages.VALUE_DATE_BEFORE_TRADE_DATE);
            } else {
                LocalDate countingValueDate = tradeDate.plusDays(plusDays);
                while (!isWorkingDay(countingValueDate)) {
                    countingValueDate = countingValueDate.plusDays(1);
                }
                if (!countingValueDate.equals(valueDate)) {
                    String mess = String.format("Value date must be %d working days after trade date", plusDays);
                    logger.error(mess);
                    result.add(mess);
                }
            }
        }
    }

    /**
     * Method for validation Option products (now only vanilla)
     * <p>
     * Conditions: TODO: put american style conditions in separate method
     * 1. tradeDate must be present
     * 2. delivery date must be present
     * 3. expiry date must be present
     * 4. premium date must be present
     * 5. expiry date must be before delivery date
     * 6. premium date must be before delivery date
     * 7. excercise start date must be present for American style Option
     * 8. excercise start date must be before expiry date for American style Option
     * 9. excercise start date must be after trade date for American style Option
     *
     * @param trade  trade to validate
     * @param result Set of errors
     */
    private void validateOptionsDates(Trade trade, Set<String> result) {
        LocalDate expiryDate = trade.getExpiryDate();
        LocalDate premiumDate = trade.getPremiumDate();
        LocalDate deliveryDate = trade.getDeliveryDate();
        LocalDate excerciseStartDate = trade.getExcerciseStartDate();

        boolean checkExcerciseStartDate = false;
        if (OptionStyle.AMERICAN.getStyleName().equals(trade.getStyle())) {
            if (excerciseStartDate == null) {
                logger.error(String.format("Trade id: %d. " + ErrorMessages.EMPTY_EXCERCISE_START_DATE, trade.getId()));
                result.add(ErrorMessages.EMPTY_EXCERCISE_START_DATE);
            } else {
                checkExcerciseStartDate = true;
            }
        }

        boolean toDoCheck = true;
        if (deliveryDate == null) {
            logger.error(String.format("Trade id: %d. " + ErrorMessages.EMPTY_DELIVERY_DATE, trade.getId()));
            result.add(ErrorMessages.EMPTY_DELIVERY_DATE);
            toDoCheck = false;
        }

        if (expiryDate == null) {
            logger.error(String.format("Trade id: %d. " + ErrorMessages.EMPTY_EXPIRY_DATE, trade.getId()));
            result.add(ErrorMessages.EMPTY_EXPIRY_DATE);
        } else if (toDoCheck) {
            if (expiryDate.isAfter(deliveryDate)) {
                logger.error(String.format("Trade id: %d. " + ErrorMessages.EXPIRY_DATE_AFTER_DELIVERY_DATE,
                        trade.getId()));
                result.add(ErrorMessages.EXPIRY_DATE_AFTER_DELIVERY_DATE);
            }
            if (checkExcerciseStartDate) {
                if (excerciseStartDate.isAfter(expiryDate)) {
                    logger.error(String.format("Trade id: %d. " + ErrorMessages.EXCERCISE_START_DATE_AFTER_EXPIRY_DATE,
                            trade.getId()));
                    result.add(ErrorMessages.EXCERCISE_START_DATE_AFTER_EXPIRY_DATE);
                }
            }
        }

        if (premiumDate == null) {
            logger.error(String.format("Trade id: %d. " + ErrorMessages.EMPTY_PREMIUM_DATE, trade.getId()));
            result.add(ErrorMessages.EMPTY_PREMIUM_DATE);
        } else if (toDoCheck) {
            if (premiumDate.isAfter(deliveryDate)) {
                logger.error(String.format("Trade id: %d. " + ErrorMessages.PREMIUM_DATE_AFTER_DELIVERY_DATE,
                        trade.getId()));
                result.add(ErrorMessages.PREMIUM_DATE_AFTER_DELIVERY_DATE);
            }
        }
        if (checkExcerciseStartDate) {
            if (trade.getTradeDate() == null) {
                logger.error(String.format("Trade id: %d. " + ErrorMessages.EMPTY_TRADE_DATE, trade.getId()));
                result.add(ErrorMessages.EMPTY_TRADE_DATE);
            } else if (excerciseStartDate.isBefore(trade.getTradeDate())) {
                logger.error(String.format("Trade id: %d. " + ErrorMessages.EXCERCISE_START_DATE_BEFORE_TRADE_DATE,
                        trade.getId()));
                result.add(ErrorMessages.EXCERCISE_START_DATE_BEFORE_TRADE_DATE);
            }
        }
    }

    /**
     * Determines whether the day is working
     *
     * @param date for checking
     * @return true if day is working
     * false if not
     */
    private boolean isWorkingDay(LocalDate date) {
        return !date.getDayOfWeek().equals(DayOfWeek.SATURDAY) &&
                !(date.getDayOfWeek().equals(DayOfWeek.SUNDAY)) &&
                !holidays.contains(date);
    }
}
