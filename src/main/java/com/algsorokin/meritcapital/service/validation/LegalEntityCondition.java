package com.algsorokin.meritcapital.service.validation;

import com.algsorokin.meritcapital.constants.ErrorMessages;
import com.algsorokin.meritcapital.entity.Trade;
import com.algsorokin.meritcapital.enums.LegalEntity;
import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Class for validation legal entity
 */
public class LegalEntityCondition implements ValidationCondition {
    private final Logger logger = Logger.getLogger(LegalEntityCondition.class);

    /**
     * Validates legalEntity field value of current trade.
     * Condition satisfied, if legalEntity contained in enum {@link LegalEntity}
     * <p>
     * ATTENTION! Validation is not case sensitive.
     * It is assumed that if spases are missing (for example CSZurich) this value will not pass validation.
     * <p>
     * If legalEntity is Empty the corresponding error text will be returned
     *
     * @param trade - trade to validate
     * @return empty Optional, if condition satisfied
     * Optional of Set of errors, if the checks fail
     */
    @Override
    public Optional<Set<String>> validate(Trade trade) {
        Set<String> result = new HashSet<>();
        if (StringUtils.isEmpty(trade.getLegalEntity())) {
            result.add(ErrorMessages.EMPTY_LE);
            logger.error(String.format("Trade id: %d. " + ErrorMessages.EMPTY_LE, trade.getId()));
        } else {

            LegalEntity foundedLegalEntity = Arrays.stream(LegalEntity.values()).filter(e -> e.getLegalEntityName().
                    equalsIgnoreCase(trade.getLegalEntity())).findAny().orElse(null);

            if (foundedLegalEntity == null) {
                String mess = String.format("Legal Entity \"%s\" is forbidden.", trade.getLegalEntity());
                logger.error(mess);
                result.add(mess);
            }

        }
        return CollectionUtils.isEmpty(result) ? Optional.empty() : Optional.of(result);
    }
}
