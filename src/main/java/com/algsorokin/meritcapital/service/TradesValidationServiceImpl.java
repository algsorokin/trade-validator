package com.algsorokin.meritcapital.service;

import com.algsorokin.meritcapital.constants.ErrorMessages;
import com.algsorokin.meritcapital.dto.ValidationResponseDto;
import com.algsorokin.meritcapital.entity.Trade;
import com.algsorokin.meritcapital.entity.Trades;
import com.algsorokin.meritcapital.enums.Status;
import com.algsorokin.meritcapital.service.validation.MainValidator;
import com.algsorokin.meritcapital.service.validation.ValidationCondition;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Implementation of validation service for trades validation
 */
@Service
public class TradesValidationServiceImpl implements TradesValidationService {

    private final Logger logger = Logger.getLogger(TradesValidationServiceImpl.class);

    /**
     * Validates trades. If trade is null, or trades list is empty, returns error result and stops validation
     * Checks all conditions represented in {@link MainValidator}
     *
     * @param trades instatce of {@link Trades} with list of trades for validation
     * @return List of {@link ValidationResponseDto} with results of validation
     */
    @Override
    public List<ValidationResponseDto> validateTrades(Trades trades) {
        final List<ValidationResponseDto> responseDtos = new ArrayList<>();

        if (trades == null || CollectionUtils.isEmpty(trades.getTest())) {
            ValidationResponseDto valDto = new ValidationResponseDto();
            logger.error(ErrorMessages.EMPTY_TRADES);
            valDto.setStatus(Status.FAIL);
            valDto.addError(ErrorMessages.EMPTY_TRADES);
            responseDtos.add(valDto);
        } else {
            trades.setIds();
            logger.info("Start to validate trades...");
            for (Trade trade : trades.getTest()) {
                logger.info("Validating trade with id: " + trade.getId());
                ValidationResponseDto responseDto = new ValidationResponseDto(trade);
                for (final ValidationCondition condition : MainValidator.getConditions()) {
                    final Optional<Set<String>> errors = condition.validate(trade);
                    errors.ifPresent(strings -> {
                        responseDto.addErrors(strings);
                        responseDto.setStatus(Status.FAIL);
                    });
                }
                if (responseDto.getStatus().equals(Status.FAIL)) {
                    logger.error("Failed to validate trade with id: " + trade.getId());
                } else {
                    logger.info("Successfully validated trade with id: " + trade.getId());
                }
                responseDtos.add(responseDto);
            }
        }
        return responseDtos;
    }
}
