package com.algsorokin.meritcapital.service;

import com.algsorokin.meritcapital.dto.ValidationResponseDto;
import com.algsorokin.meritcapital.entity.Trades;

import java.util.List;

/**
 * Interface for all validation services. Can have different implementation for different situations
 */
public interface TradesValidationService {

    /**
     * Validates trades
     *
     * @param trades instatce of {@link Trades} with list of trades for validation
     * @return List of {@link ValidationResponseDto} with results of validation
     */
    List<ValidationResponseDto> validateTrades(Trades trades);
}
