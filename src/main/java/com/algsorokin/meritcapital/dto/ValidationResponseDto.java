package com.algsorokin.meritcapital.dto;

import com.algsorokin.meritcapital.entity.Trade;
import com.algsorokin.meritcapital.enums.Status;

import java.util.HashSet;
import java.util.Set;

/**
 * Dto represents validation results of one trade
 */
public class ValidationResponseDto {

    /**
     * Status from {@link Status} (FAIL OR SUCCESS)
     */
    private Status status;

    /**
     * Validated trade
     */
    private String trade;

    /**
     * Set of validation errors
     */
    private HashSet<String> errors = new HashSet<>();

    public ValidationResponseDto() {
        this.status = Status.SUCCESS;
    }

    public ValidationResponseDto(Trade trade) {
        this();
        this.trade = trade.toString();
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public HashSet<String> getErrors() {
        return errors;
    }

    public void setErrors(HashSet<String> errors) {
        this.errors = errors;
    }

    /**
     * Adds error to set.
     *
     * @param error - error message.
     */
    public void addError(String error) {
        errors.add(error);
    }

    /**
     * Adds set of errors to set.
     *
     * @param errorSet - set of error messages.
     */
    public void addErrors(Set<String> errorSet) {
        errors.addAll(errorSet);
    }

    public String getTrade() {
        return trade;
    }

    public void setTrade(String trade) {
        this.trade = trade;
    }
}
