package com.algsorokin.meritcapital.enums;

/**
 * Enum to store possible values of option styles
 */
public enum OptionStyle {
    EUROPEAN("EUROPEAN"),
    AMERICAN("AMERICAN");

    private String styleName;

    private OptionStyle(String styleName) {
        this.styleName = styleName;
    }

    public String getStyleName() {
        return styleName;
    }
}
