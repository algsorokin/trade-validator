package com.algsorokin.meritcapital.enums;

/**
 * Enum to store possible values of trade type
 */
public enum TradeType {
    SPOT("Spot"),
    FORWARD("Forward"),
    VANILLAOPTION("VanillaOption");

    private String typeName;

    private TradeType(String typeName) {
        this.typeName = typeName;
    }

    public String getTypeName() {
        return typeName;
    }
}

