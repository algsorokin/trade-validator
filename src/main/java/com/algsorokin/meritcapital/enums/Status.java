package com.algsorokin.meritcapital.enums;

/**
 * Enum to store possible response statuses
 */
public enum Status {
    SUCCESS, FAIL
}
