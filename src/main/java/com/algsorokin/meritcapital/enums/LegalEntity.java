package com.algsorokin.meritcapital.enums;

/**
 * Enum to store possible values of legal entity
 */
public enum LegalEntity {
    CSZURICH("CS Zurich");


    private String legalEntityName;

    private LegalEntity(String legalEntityName) {
        this.legalEntityName = legalEntityName;
    }

    public String getLegalEntityName() {
        return legalEntityName;
    }
}