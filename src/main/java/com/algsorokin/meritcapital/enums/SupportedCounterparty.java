package com.algsorokin.meritcapital.enums;

/**
 * Enum to store possible values of counterparties (customers)
 */
public enum SupportedCounterparty {
    PLUTO1("PLUTO1"),
    PLUTO2("PLUTO2");

    private String customer;

    private SupportedCounterparty(String customer) {
        this.customer = customer;
    }

    public String getCustomer() {
        return customer;
    }
}
