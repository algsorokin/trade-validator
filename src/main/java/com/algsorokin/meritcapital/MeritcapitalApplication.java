package com.algsorokin.meritcapital;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Application entry point
 */
@SpringBootApplication
public class MeritcapitalApplication {

    public static void main(String[] args) {
        SpringApplication.run(MeritcapitalApplication.class, args);
    }

}
