package com.algsorokin.meritcapital.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Class for parsing json with holidays from resources to Set
 */
public class HolidayParser {
    private final Logger logger = Logger.getLogger(HolidayParser.class);

    /**
     * Class for parsing json file with dates in "yyyy-MM-dd" format
     *
     * @param path to json file storing holidays
     * @return Set of LocalDate
     */
    public Set<LocalDate> parseHolidays(String path) {
        Set<LocalDate> result = new HashSet<>();
        try {
            logger.info("Trying to parse holiday dates from " + path);
            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            InputStream holidaysData = classloader.getResourceAsStream(path);

            ObjectMapper objectMapper = new ObjectMapper();
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            objectMapper.setDateFormat(df);
            HashSet<Date> holidays = objectMapper.readValue(holidaysData, new TypeReference<HashSet<Date>>() {
            });
            holidays.forEach(date -> result.add(date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()));
        } catch (Exception e) {
            logger.error(String.format("Fail to parse holiday dates from %s. Validation will continue without holiday " +
                    "dates...", path));
            e.printStackTrace();
        }
        return result;
    }
}
