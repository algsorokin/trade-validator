package com.algsorokin.meritcapital.utils;

import com.algsorokin.meritcapital.enums.TradeType;

import java.util.Arrays;

/**
 * Class helper for searching {@link TradeType} value
 */
public class TradeTypeUtil {

    /**
     * @param type tradeType from Trade
     * @return founded enum {@link TradeType} value or null, if trade type is not found
     */
    public static TradeType foundTradeType(String type) {
        return Arrays.stream(TradeType.values()).filter(e -> e.getTypeName().
                equalsIgnoreCase(type)).findAny().orElse(null);
    }
}
