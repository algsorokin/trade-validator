package com.algsorokin.meritcapital.utils;

import com.algsorokin.meritcapital.service.validation.CcyCondition;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class for receiving currency codes from web
 */
public class CcyIsoOrgParser {
    private final Logger logger = Logger.getLogger(CcyCondition.class);
    private final Pattern regexFormat = Pattern.compile(".*(<Ccy>)([A-Z]{3})(</Ccy>).*");

    /**
     * Parse file (https://www.currency-iso.org/dam/downloads/lists/list_one.xml) to find all currency codes
     * @return Set of currency codes if success
     *  Empty set if fail
     */
    public Set<String> parseCcyFromIsoOrg() {
        Set<String> result = new HashSet<>();
        try {
            URL IsoOrg = new URL("https://www.currency-iso.org/dam/downloads/lists/list_one.xml");
            URLConnection yc = IsoOrg.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                Matcher matcher = regexFormat.matcher(inputLine);
                if (matcher.matches())
                    result.add(matcher.group(2));
            }
            in.close();
        } catch (Exception e) {
            logger.error("Failed to load currency codes...");
        }
        return result;
    }
}
