package com.algsorokin.meritcapital.controllers;

import com.algsorokin.meritcapital.dto.ValidationResponseDto;
import com.algsorokin.meritcapital.entity.Trades;
import com.algsorokin.meritcapital.service.TradesValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Class represents controllers for validation service
 */
@Controller
@RequestMapping("/")
public class TradesController {

    @Autowired
    TradesValidationService tradesValidationService;

    /**
     * Redirect from localhost:8080 to localhost:8080/validation
     *
     * @return path to validation.ftl form
     */
    @GetMapping("/")
    public String goToValidationForm() {
        return "validation";
    }

    /**
     * Main controller for sending trades from json to validation service
     * adds list of {@link ValidationResponseDto} with validation results to Model for using in freemarker template
     *
     * @param trades trades from input json
     * @return redirection to responseList.ftl
     */
    @PostMapping("/validate")
    public String createOrganisationAndSendBid(@RequestBody Trades trades, Model model) {
        List<ValidationResponseDto> res = tradesValidationService.validateTrades(trades);
        model.addAttribute("trades", res);

        return "responseList";
    }
}
