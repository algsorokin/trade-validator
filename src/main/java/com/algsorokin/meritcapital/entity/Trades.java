package com.algsorokin.meritcapital.entity;

import java.util.List;

/**
 * Class describing the structure of the input json-file with trades
 */
public class Trades {
    /**
     * List of trades representing array of trades from input json-file
     */
    private List<Trade> test;

    public List<Trade> getTest() {
        return test;
    }

    public void setTest(List<Trade> test) {
        this.test = test;
    }

    /**
     * Assignment id for all trades
     */
    public void setIds() {
        int i = 0;
        for (Trade trade : test) {
            trade.setId(++i);
        }
    }
}
