package com.algsorokin.meritcapital.constants;

/**
 * Text constants for error messages
 */
public class ErrorMessages {
    public static final String EMPTY_TRADES = "Trades list are empty! Please try again with valid json";

    public static final String EMPTY_LE = "Legal Entity is empty.";
    public static final String EMPTY_COUNTERPARTY = "Customer is empty.";
    public static final String EMPTY_TRADE_TYPE = "Trade type is empty.";
    public static final String EMPTY_OPTION_STYLE = "Option style is empty.";
    public static final String EMPTY_CCY_PAIR = "CcyPair is empty.";
    public static final String NOT_VALID_CCY_PAIR = "CcyPair is not valid.";

    public static final String EMPTY_TRADE_DATE = "Trade date is empty.";
    public static final String EMPTY_VALUE_DATE = "Value date must be present for types Spot and Forward.";
    public static final String VALUE_DATE_BEFORE_TRADE_DATE = "Value date must be after trade date.";
    public static final String NOT_WORKING_VALUE_DATE = "Value date must be working day.";

    public static final String EMPTY_DELIVERY_DATE = "Delivery date must be present for Option products.";
    public static final String EMPTY_EXPIRY_DATE = "Expiry date must be present for Option products.";
    public static final String EXPIRY_DATE_AFTER_DELIVERY_DATE = "Expiry date must be before delivery date.";
    public static final String EMPTY_PREMIUM_DATE = "Premium date must be present for Option products.";
    public static final String PREMIUM_DATE_AFTER_DELIVERY_DATE = "Premium date must be before delivery date.";

    public static final String EMPTY_EXCERCISE_START_DATE = "Excercise date must be present for American style Option " +
            "products.";
    public static final String EXCERCISE_START_DATE_AFTER_EXPIRY_DATE = "Excercise start date must be before expiry date";
    public static final String EXCERCISE_START_DATE_BEFORE_TRADE_DATE = "Excercise start date must be after trade date";
}
