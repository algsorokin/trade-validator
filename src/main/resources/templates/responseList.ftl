<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
    </style>
</head>
<body>
<a href="/">Back to validation page</a>

<h3>Trade list</h3>
<table style="width:100%">
    <tr>
        <th>Status</th>
        <th>Errors</th>
        <th>Trade</th>
    </tr>
    <#list trades as result>
        <tr>
            <td>${result.status}</td>
            <td>
            <#list result.errors as error>
                ${error}
            </#list>
            </td>
            <td>${result.trade}"</td>
        <tr>
    </#list>

</table>
</body>
</html>