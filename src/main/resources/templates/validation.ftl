<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" charset=" UTF-8">
    <title>Validation page</title>
    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
</head>
<body>

<form name="test" class="trades" action="/validate" method="POST" enctype="text/plain">
    <h3>Please insert your valid json with trades here (not valid json returns nothing)</h3>
    <textarea
            title="Trades"
            name="test"
            id="jsonString"
            style="width: 100%; height: 500px;"
    >
        {"test":[{"customer":"PLUTO1","ccyPair":"EURUSD","type":"Spot","direction":"BUY","tradeDate":"2016-08-11","amount1":1000000,"amount2":1120000,"rate":1.12,"valueDate":"2016-08-15","legalEntity":"CS Zurich","trader":"JohannBaumfiddler"},
        {"customer":"PLUTO1","ccyPair":"EURUSD","type":"Spot","direction":"SELL","tradeDate":"2016-08-11","amount1":1000000,"amount2":1120000,"rate":1.12,"valueDate":"2016-08-22","legalEntity":"CS Zurich","trader":"JohannBaumfiddler"},
        {"customer":"PLUTO2","ccyPair":"EURUSD","type":"Forward","direction":"SELL","tradeDate":"2016-08-11","amount1":1000000,"amount2":1120000,"rate":1.12,"valueDate":"2016-08-22","legalEntity":"CS Zurich","trader":"JohannBaumfiddler"},
        {"customer":"PLUTO2","ccyPair":"EURUSD","type":"Forward","direction":"BUY","tradeDate":"2016-08-11","amount1":1000000,"amount2":1120000,"rate":1.12,"valueDate":"2016-08-21","legalEntity":"CS Zurich","trader":"JohannBaumfiddler"},
        {"customer":"PLUTO2","ccyPair":"EURUSD","type":"Forward","direction":"BUY","tradeDate":"2016-08-11","amount1":1000000,"amount2":1120000,"rate":1.12,"valueDate":"2016-08-08","legalEntity":"CS Zurich","trader":"JohannBaumfiddler"},
        {"customer":"PLUT02","ccyPair":"EURUSD","type":"Forward","direction":"BUY","tradeDate":"2016-08-11","amount1":1000000,"amount2":1120000,"rate":1.12,"valueDate":"2016-08-08","legalEntity":"CS Zurich","trader":"JohannBaumfiddler"},
        {"customer":"PLUTO3","ccyPair":"EURUSD","type":"Forward","direction":"BUY","tradeDate":"2016-08-11","amount1":1000000,"amount2":1120000,"rate":1.12,"valueDate":"2016-08-22","legalEntity":"CS Zurich","trader":"JohannBaumfiddler"},
        {"customer":"PLUTO1","ccyPair":"EURUSD","type":"VanillaOption","style":"EUROPEAN","direction":"BUY","strategy":"CALL","tradeDate":"2016-08-11","amount1":1000000,"amount2":1120000,"rate":1.12,"deliveryDate":"2016-08-22","expiryDate":"2016-08-19","payCcy":"USD","premium":0.2,"premiumCcy":"USD","premiumType":"%USD","premiumDate":"2016-08-12","legalEntity":"CSZurich","trader":"Johann Baumfiddler"},
        {"customer":"PLUTO2","ccyPair":"EURUSD","type":"VanillaOption","style":"EUROPEAN","direction":"SELL","strategy":"CALL","tradeDate":"2016-08-11","amount1":1000000,"amount2":1120000,"rate":1.12,"deliveryDate":"2016-08-22","expiryDate":"2016-08-21","payCcy":"USD","premium":0.2,"premiumCcy":"USD","premiumType":"%USD","premiumDate":"2016-08-12","legalEntity":"CSZurich","trader":"Johann Baumfiddler"},
        {"customer":"PLUTO1","ccyPair":"EURUSD","type":"VanillaOption","style":"EUROPEAN","direction":"BUY","strategy":"CALL","tradeDate":"2016-08-11","amount1":1000000,"amount2":1120000,"rate":1.12,"deliveryDate":"2016-08-22","expiryDate":"2016-08-25","payCcy":"USD","premium":0.2,"premiumCcy":"USD","premiumType":"%USD","premiumDate":"2016-08-12","legalEntity":"CSZurich","trader":"Johann Baumfiddler"},
        {"customer":"PLUTO1","ccyPair":"EURUSD","type":"VanillaOption","style":"AMERICAN","direction":"BUY","strategy":"CALL","tradeDate":"2016-08-11","amount1":1000000,"amount2":1120000,"rate":1.12,"deliveryDate":"2016-08-22","expiryDate":"2016-08-19","excerciseStartDate":"2016-08-12","payCcy":"USD","premium":0.2,"premiumCcy":"USD","premiumType":"%USD","premiumDate":"2016-08-12","legalEntity":"CS Zurich","trader":"Johann Baumfiddler"},
        {"customer":"PLUTO2","ccyPair":"EURUSD","type":"VanillaOption","style":"AMERICAN","direction":"SELL","strategy":"CALL","tradeDate":"2016-08-11","amount1":1000000,"amount2":1120000,"rate":1.12,"deliveryDate":"2016-08-22","expiryDate":"2016-08-21","excerciseStartDate":"2016-08-12","payCcy":"USD","premium":0.2,"premiumCcy":"USD","premiumType":"%USD","premiumDate":"2016-08-12","legalEntity":"CS Zurich","trader":"Johann Baumfiddler"},
        {"customer":"PLUTO1","ccyPair":"EURUSD","type":"VanillaOption","style":"AMERICAN","direction":"BUY","strategy":"CALL","tradeDate":"2016-08-11","amount1":1000000,"amount2":1120000,"rate":1.12,"deliveryDate":"2016-08-22","expiryDate":"2016-08-25","excerciseStartDate":"2016-08-12","payCcy":"USD","premium":0.2,"premiumCcy":"USD","premiumType":"%USD","premiumDate":"2016-08-12","legalEntity":"CS Zurich","trader":"Johann Baumfiddler"},
        {"customer":"PLUTO1","ccyPair":"EURUSD","type":"VanillaOption","style":"AMERICAN","direction":"BUY","strategy":"CALL","tradeDate":"2016-08-11","amount1":1000000,"amount2":1120000,"rate":1.12,"deliveryDate":"2016-08-22","expiryDate":"2016-08-19","excerciseStartDate":"2016-08-10","payCcy":"USD","premium":0.2,"premiumCcy":"USD","premiumType":"%USD","premiumDate":"2016-08-12","legalEntity":"CS Zurich","trader":"Johann Baumfiddler"},
        {"customer":"PLUTO3","ccyPair":"EURUSD","type":"VanillaOption","style":"AMERICAN","direction":"SELL","strategy":"CALL","tradeDate":"2016-08-11","amount1":1000000,"amount2":1120000,"rate":1.12,"deliveryDate":"2016-08-22","expiryDate":"2016-08-19","excerciseStartDate":"2016-08-10","payCcy":"USD","premium":0.2,"premiumCcy":"USD","premiumType":"%USD","premiumDate":"2016-08-12","legalEntity":"CS Zurich","trader":"Johann Baumfiddler"}]}'
    </textarea>
    <input type="submit" value="Validate trades">
    <button id="shutDown" type="button" class="button">Shutdown App</button>
</form>
<br>

<script>
    document.querySelector('.trades').addEventListener('submit', function (e) {
        e.preventDefault();

        var jsonTextArea = document.getElementById("jsonString");

        $.ajax({
            url: '/validate',
            type: "POST",
            data: jsonTextArea.value,
            contentType: "application/json",
        }).done(function (data, textStatus, jqXHR) {
            $("body").html(data)
        });
    });
    document.querySelector('#shutDown').addEventListener('click', function (e) {
        e.preventDefault();

        $.ajax({
            url: '/actuator/shutdown',
            type: "POST",
            contentType: "application/json"
            //complete: callback
        });
    })
</script>

</body>
</html>