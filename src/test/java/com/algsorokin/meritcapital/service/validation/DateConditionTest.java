package com.algsorokin.meritcapital.service.validation;

import com.algsorokin.meritcapital.constants.ErrorMessages;
import com.algsorokin.meritcapital.entity.Trade;
import com.algsorokin.meritcapital.enums.OptionStyle;
import com.algsorokin.meritcapital.enums.TradeType;
import org.junit.Test;

import java.time.LocalDate;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Tests for {@link DateCondition}
 */
public class DateConditionTest {
    DateCondition dateCondition = new DateCondition("holidays.json");

    @Test
    public void emptyTradeTypeErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        Optional<Set<String>> errors = dateCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains(ErrorMessages.EMPTY_TRADE_TYPE));
        assertEquals(errors.get().size(), 1);
    }

    @Test
    public void wrongTradeTypeErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setType("Optionium");
        Optional<Set<String>> errors = dateCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains(String.format("Trade type \"%s\" is forbidden.", trade.getType())));
        assertEquals(errors.get().size(), 1);
    }

    /**
     * Tests for valueDateCheckProducts (now Spot and Forward)
     */

    @Test
    public void emptyTradeDateValueDateProductsErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setType(TradeType.SPOT.getTypeName());
        trade.setValueDate(LocalDate.of(2017, 7, 21));
        Optional<Set<String>> errors = dateCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains(ErrorMessages.EMPTY_TRADE_DATE));
        assertEquals(errors.get().size(), 1);
    }

    @Test
    public void emptyValueDateErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setType(TradeType.SPOT.getTypeName());
        trade.setTradeDate(LocalDate.of(2017, 7, 18));
        Optional<Set<String>> errors = dateCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains(ErrorMessages.EMPTY_VALUE_DATE));
        assertEquals(errors.get().size(), 1);
    }

    @Test
    public void notWorkingValueDateErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setType(TradeType.SPOT.getTypeName());
        trade.setTradeDate(LocalDate.of(2017, 7, 18));
        trade.setValueDate(LocalDate.of(2017, 7, 22));
        Optional<Set<String>> errors = dateCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains(ErrorMessages.NOT_WORKING_VALUE_DATE));
        assertEquals(errors.get().size(), 2);
    }

    @Test
    public void holidayValueDateErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setType(TradeType.SPOT.getTypeName());
        trade.setTradeDate(LocalDate.of(2018, 12, 28));
        trade.setValueDate(LocalDate.of(2019, 1, 2));
        Optional<Set<String>> errors = dateCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains(ErrorMessages.NOT_WORKING_VALUE_DATE));
        assertEquals(errors.get().size(), 2);
    }

    @Test
    public void valueDateBeforeTradeDateErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setType(TradeType.SPOT.getTypeName());
        trade.setTradeDate(LocalDate.of(2017, 7, 18));
        trade.setValueDate(LocalDate.of(2017, 6, 22));
        Optional<Set<String>> errors = dateCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains(ErrorMessages.VALUE_DATE_BEFORE_TRADE_DATE));
        assertEquals(errors.get().size(), 1);
    }

    @Test
    public void valueDateCountingForSpotErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setType(TradeType.SPOT.getTypeName());
        trade.setTradeDate(LocalDate.of(2017, 7, 18));
        trade.setValueDate(LocalDate.of(2017, 7, 21));
        Optional<Set<String>> errors = dateCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains(String.format("Value date must be 2 working days after trade date")));
        assertEquals(errors.get().size(), 1);
    }

    @Test
    public void valueDateCountingForForwardErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setType(TradeType.FORWARD.getTypeName());
        trade.setTradeDate(LocalDate.of(2017, 7, 18));
        trade.setValueDate(LocalDate.of(2017, 7, 24));
        Optional<Set<String>> errors = dateCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains(String.format("Value date must be 7 working days after trade date")));
        assertEquals(errors.get().size(), 1);
    }

    @Test
    public void successValidationOfSpotDatesTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setType(TradeType.SPOT.getTypeName());
        trade.setTradeDate(LocalDate.of(2017, 7, 20));
        trade.setValueDate(LocalDate.of(2017, 7, 24));
        Optional<Set<String>> errors = dateCondition.validate(trade);
        assertFalse(errors.isPresent());
    }

    @Test
    public void successValidationOfForwardDatesTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setType(TradeType.FORWARD.getTypeName());
        trade.setTradeDate(LocalDate.of(2017, 7, 18));
        trade.setValueDate(LocalDate.of(2017, 7, 25));
        Optional<Set<String>> errors = dateCondition.validate(trade);
        assertFalse(errors.isPresent());
    }

    /**
     * Tests for optionsCheckProducts (now Vanilla)
     */
    @Test
    public void emptyExcerciseStartDateErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setType(TradeType.VANILLAOPTION.getTypeName());
        trade.setStyle(OptionStyle.AMERICAN.getStyleName());
        trade.setDeliveryDate(LocalDate.of(2016, 8, 21));
        trade.setExpiryDate(LocalDate.of(2016, 8, 20));
        trade.setPremiumDate(LocalDate.of(2016, 8, 20));
        Optional<Set<String>> errors = dateCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains(ErrorMessages.EMPTY_EXCERCISE_START_DATE));
        assertEquals(errors.get().size(), 1);
    }

    @Test
    public void emptyDeliveryDateErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setType(TradeType.VANILLAOPTION.getTypeName());
        trade.setStyle(OptionStyle.EUROPEAN.getStyleName());
        trade.setExpiryDate(LocalDate.of(2016, 8, 20));
        trade.setPremiumDate(LocalDate.of(2016, 8, 20));
        Optional<Set<String>> errors = dateCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains(ErrorMessages.EMPTY_DELIVERY_DATE));
        assertEquals(errors.get().size(), 1);
    }

    @Test
    public void emptyExpiryDateErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setType(TradeType.VANILLAOPTION.getTypeName());
        trade.setStyle(OptionStyle.EUROPEAN.getStyleName());
        trade.setDeliveryDate(LocalDate.of(2016, 8, 19));
        trade.setPremiumDate(LocalDate.of(2016, 8, 18));
        Optional<Set<String>> errors = dateCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains(ErrorMessages.EMPTY_EXPIRY_DATE));
        assertEquals(errors.get().size(), 1);
    }

    @Test
    public void expiryDateAfterDeliveryDateErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setType(TradeType.VANILLAOPTION.getTypeName());
        trade.setStyle(OptionStyle.EUROPEAN.getStyleName());
        trade.setPremiumDate(LocalDate.of(2016, 8, 18));
        trade.setDeliveryDate(LocalDate.of(2016, 8, 19));
        trade.setExpiryDate(LocalDate.of(2016, 8, 20));
        Optional<Set<String>> errors = dateCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains(ErrorMessages.EXPIRY_DATE_AFTER_DELIVERY_DATE));
        assertEquals(errors.get().size(), 1);
    }

    @Test
    public void excerciseStartDateAfterExpiryDateErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setType(TradeType.VANILLAOPTION.getTypeName());
        trade.setStyle(OptionStyle.AMERICAN.getStyleName());
        trade.setPremiumDate(LocalDate.of(2016, 8, 18));
        trade.setDeliveryDate(LocalDate.of(2016, 8, 19));
        trade.setExpiryDate(LocalDate.of(2016, 8, 17));
        trade.setExcerciseStartDate(LocalDate.of(2016, 8, 18));
        trade.setTradeDate(LocalDate.of(2016, 8, 17));
        Optional<Set<String>> errors = dateCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains(ErrorMessages.EXCERCISE_START_DATE_AFTER_EXPIRY_DATE));
        assertEquals(errors.get().size(), 1);
    }

    @Test
    public void emptyPremiumDateErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setType(TradeType.VANILLAOPTION.getTypeName());
        trade.setStyle(OptionStyle.EUROPEAN.getStyleName());
        trade.setDeliveryDate(LocalDate.of(2016, 8, 19));
        trade.setExpiryDate(LocalDate.of(2016, 8, 17));
        Optional<Set<String>> errors = dateCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains(ErrorMessages.EMPTY_PREMIUM_DATE));
        assertEquals(errors.get().size(), 1);
    }

    @Test
    public void premiumDateAfterDeliveryDateErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setType(TradeType.VANILLAOPTION.getTypeName());
        trade.setStyle(OptionStyle.EUROPEAN.getStyleName());
        trade.setDeliveryDate(LocalDate.of(2016, 8, 19));
        trade.setExpiryDate(LocalDate.of(2016, 8, 17));
        trade.setPremiumDate(LocalDate.of(2016, 8, 20));
        Optional<Set<String>> errors = dateCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains(ErrorMessages.PREMIUM_DATE_AFTER_DELIVERY_DATE));
        assertEquals(errors.get().size(), 1);
    }

    @Test
    public void excerciseStartDateBeforeTradeDateErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setType(TradeType.VANILLAOPTION.getTypeName());
        trade.setStyle(OptionStyle.AMERICAN.getStyleName());
        trade.setDeliveryDate(LocalDate.of(2016, 8, 21));
        trade.setExpiryDate(LocalDate.of(2016, 8, 20));
        trade.setExcerciseStartDate(LocalDate.of(2016, 8, 16));
        trade.setPremiumDate(LocalDate.of(2016, 8, 20));
        trade.setTradeDate(LocalDate.of(2016, 8, 17));
        Optional<Set<String>> errors = dateCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains(ErrorMessages.EXCERCISE_START_DATE_BEFORE_TRADE_DATE));
        assertEquals(errors.get().size(), 1);
    }

    @Test
    public void emptyTradeDateOptionTypeProductsErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setType(TradeType.VANILLAOPTION.getTypeName());
        trade.setStyle(OptionStyle.AMERICAN.getStyleName());
        trade.setDeliveryDate(LocalDate.of(2016, 8, 21));
        trade.setExpiryDate(LocalDate.of(2016, 8, 20));
        trade.setExcerciseStartDate(LocalDate.of(2016, 8, 16));
        trade.setPremiumDate(LocalDate.of(2016, 8, 20));
        Optional<Set<String>> errors = dateCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains(ErrorMessages.EMPTY_TRADE_DATE));
        assertEquals(errors.get().size(), 1);
    }

    @Test
    public void successAmericanStyleOptionValidationTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setType(TradeType.VANILLAOPTION.getTypeName());
        trade.setStyle(OptionStyle.AMERICAN.getStyleName());
        trade.setTradeDate(LocalDate.of(2016, 8, 11));
        trade.setDeliveryDate(LocalDate.of(2016, 8, 22));
        trade.setExpiryDate(LocalDate.of(2016, 8, 19));
        trade.setExcerciseStartDate(LocalDate.of(2016, 8, 16));
        trade.setPremiumDate(LocalDate.of(2016, 8, 12));
        Optional<Set<String>> errors = dateCondition.validate(trade);
        assertFalse(errors.isPresent());
    }

    @Test
    public void successEuropeanStyleOptionValidationTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setType(TradeType.VANILLAOPTION.getTypeName());
        trade.setStyle(OptionStyle.EUROPEAN.getStyleName());
        trade.setTradeDate(LocalDate.of(2016, 8, 11));
        trade.setDeliveryDate(LocalDate.of(2016, 8, 22));
        trade.setExpiryDate(LocalDate.of(2016, 8, 19));
        trade.setPremiumDate(LocalDate.of(2016, 8, 12));
        Optional<Set<String>> errors = dateCondition.validate(trade);
        assertFalse(errors.isPresent());
    }
}