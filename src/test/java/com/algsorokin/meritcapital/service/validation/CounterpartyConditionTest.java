package com.algsorokin.meritcapital.service.validation;

import com.algsorokin.meritcapital.constants.ErrorMessages;
import com.algsorokin.meritcapital.entity.Trade;
import org.junit.Test;

import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests for {@link CounterpartyCondition}
 */
public class CounterpartyConditionTest {

    private static final CounterpartyCondition counterpartyCondition = new CounterpartyCondition();

    @Test
    public void emptyConterpartyErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        Optional<Set<String>> errors = counterpartyCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains(ErrorMessages.EMPTY_COUNTERPARTY));
    }

    @Test
    public void notValidConterpartyErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setCustomer("Kolyan");
        Optional<Set<String>> errors = counterpartyCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains(String.format("Counterparty \"%s\" is forbidden.", trade.getCustomer())));
    }

    /**
     * It is assumed that customer name is not case sensitive
     */
    @Test
    public void validConterpartyNotCaseSensitiveTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setCustomer("pLuTO1");
        Optional<Set<String>> errors = counterpartyCondition.validate(trade);
        assertFalse(errors.isPresent());
    }
}