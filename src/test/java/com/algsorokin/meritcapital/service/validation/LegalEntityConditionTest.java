package com.algsorokin.meritcapital.service.validation;

import com.algsorokin.meritcapital.constants.ErrorMessages;
import com.algsorokin.meritcapital.entity.Trade;
import org.junit.Test;

import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests for {@link LegalEntityCondition}
 */
public class LegalEntityConditionTest {
    private static final LegalEntityCondition legalEntityCondition = new LegalEntityCondition();

    @Test
    public void emptyLegalEntityErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        Optional<Set<String>> errors = legalEntityCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains(ErrorMessages.EMPTY_LE));
    }

    @Test
    public void notValidLegalEntityErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setLegalEntity("Roga i kopyta");
        Optional<Set<String>> errors = legalEntityCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains(String.format("Legal Entity \"%s\" is forbidden.", trade.getLegalEntity())));
    }

    /**
     * It is assumed that legal entity is not case sensitive
     */
    @Test
    public void validLegalEntityConditionNotCaseSensitiveTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setLegalEntity("cS zUrIcH");
        Optional<Set<String>> errors = legalEntityCondition.validate(trade);
        assertFalse(errors.isPresent());
    }

    /**
     * It is assumed that if spases are missing (for example CSZurich) this value will not pass validation
     */
    @Test
    public void legalEntityConditionWithSpaceErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setLegalEntity("CSZurich");
        Optional<Set<String>> errors = legalEntityCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains(String.format("Legal Entity \"%s\" is forbidden.", trade.getLegalEntity())));
    }
}
