package com.algsorokin.meritcapital.service.validation;

import com.algsorokin.meritcapital.constants.ErrorMessages;
import com.algsorokin.meritcapital.entity.Trade;
import org.junit.Test;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests for {@link CcyCondition}
 */
public class CcyConditionTest {

    private static final CcyCondition ccySimpleCondition = new CcyCondition();

    @Test
    public void emptyCcyPairErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        Optional<Set<String>> errors = ccySimpleCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains(ErrorMessages.EMPTY_CCY_PAIR));
    }

    @Test
    public void notValidCcyPairErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setCcyPair("EUROUSDI");
        Optional<Set<String>> errors = ccySimpleCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains(ErrorMessages.NOT_VALID_CCY_PAIR));
    }

    @Test
    public void validCcyPairTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setCcyPair("EURUSD");
        Optional<Set<String>> errors = ccySimpleCondition.validate(trade);
        assertFalse(errors.isPresent());
    }

    @Test
    public void validCcyPairFromAdditionalCurrTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setCcyPair("MRUSTN");
        Optional<Set<String>> errors = ccySimpleCondition.validate(trade);
        assertFalse(errors.isPresent());
    }

    /**
     * It is assumed that ccy codes must be in upper case
     */
    @Test
    public void lowerCaseCcyPairErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setCcyPair("eURUSD");
        Optional<Set<String>> errors = ccySimpleCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains("Currency code \"eUR\" in field \"baseCcy\" is not not " +
                "compliant with ISO 4217"));

    }

    @Test
    public void validPayCcyTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setCcyPair("EURUSD");
        trade.setPayCcy("RUB");
        Optional<Set<String>> errors = ccySimpleCondition.validate(trade);
        assertFalse(errors.isPresent());
    }

    @Test
    public void notValidPayCcyErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setCcyPair("EURUSD");
        trade.setPayCcy("TT");
        Optional<Set<String>> errors = ccySimpleCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains("Currency code \"TT\" in field \"payCcy\" is not not " +
                "compliant with ISO 4217"));

    }

    @Test
    public void validPremiumCcyTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setCcyPair("EURUSD");
        trade.setPremiumCcy("RUB");
        Optional<Set<String>> errors = ccySimpleCondition.validate(trade);
        assertFalse(errors.isPresent());
    }

    @Test
    public void notValidPremiumCcyErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setCcyPair("EURUSD");
        trade.setPremiumCcy("RRR");
        Optional<Set<String>> errors = ccySimpleCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains("Currency code \"RRR\" in field \"premiumCcy\" is not not " +
                "compliant with ISO 4217"));
    }
}
