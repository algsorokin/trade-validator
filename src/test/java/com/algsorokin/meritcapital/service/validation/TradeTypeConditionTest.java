package com.algsorokin.meritcapital.service.validation;

import com.algsorokin.meritcapital.constants.ErrorMessages;
import com.algsorokin.meritcapital.entity.Trade;
import com.algsorokin.meritcapital.enums.TradeType;
import org.junit.Test;

import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests for {@link TradeTypeCondition}
 */
public class TradeTypeConditionTest {
    TradeTypeCondition tradeTypeCondition = new TradeTypeCondition();

    @Test
    public void emptyTradeTypeErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        Optional<Set<String>> errors = tradeTypeCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains(ErrorMessages.EMPTY_TRADE_TYPE));
    }

    @Test
    public void notValidTradeTypeErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setType("Optionium");
        Optional<Set<String>> errors = tradeTypeCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains(String.format("Trade type \"%s\" is forbidden.", trade.getType())));
    }

    /**
     * It is assumed that trade type is not case sensitive
     */
    @Test
    public void validTradeConditionNotCaseSensitiveTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setType("fOrWard");
        Optional<Set<String>> errors = tradeTypeCondition.validate(trade);
        assertFalse(errors.isPresent());
    }

    @Test
    public void emptyOptionStyleErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setType(TradeType.VANILLAOPTION.getTypeName());
        Optional<Set<String>> errors = tradeTypeCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains(ErrorMessages.EMPTY_OPTION_STYLE));

    }

    @Test
    public void wrongOptionStyleErrorTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setType(TradeType.VANILLAOPTION.getTypeName());
        trade.setStyle("Martian");
        Optional<Set<String>> errors = tradeTypeCondition.validate(trade);
        assertTrue(errors.isPresent());
        assertTrue(errors.get().contains(String.format("Option style \"%s\" is forbidden.", trade.getStyle())));

    }

    /**
     * It is assumed that trade style is not case sensitive
     */
    @Test
    public void successOptionTypeValidationTest() {
        Trade trade = new Trade();
        trade.setId(1);
        trade.setType(TradeType.VANILLAOPTION.getTypeName());
        trade.setStyle("aMeRiCan");
        Optional<Set<String>> errors = tradeTypeCondition.validate(trade);
        assertFalse(errors.isPresent());
    }
}