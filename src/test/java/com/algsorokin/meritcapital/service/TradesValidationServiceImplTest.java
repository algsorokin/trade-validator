package com.algsorokin.meritcapital.service;

import com.algsorokin.meritcapital.constants.ErrorMessages;
import com.algsorokin.meritcapital.dto.ValidationResponseDto;
import com.algsorokin.meritcapital.entity.Trades;
import org.junit.Test;
import org.springframework.util.CollectionUtils;

import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests for {@link TradesValidationServiceImpl}
 */
public class TradesValidationServiceImplTest {
    TradesValidationServiceImpl tradesValidationService = new TradesValidationServiceImpl();

    @Test
    public void EmptyTradesErrorTest() {
        List<ValidationResponseDto> result = tradesValidationService.validateTrades(null);
        assertTrue(!CollectionUtils.isEmpty(result));
        assertNotNull(result.get(0));
        assertTrue(!CollectionUtils.isEmpty(result.get(0).getErrors()));
        assertTrue(result.get(0).getErrors().contains(ErrorMessages.EMPTY_TRADES));

        result = tradesValidationService.validateTrades(new Trades());
        assertTrue(!CollectionUtils.isEmpty(result));
        assertNotNull(result.get(0));
        assertTrue(!CollectionUtils.isEmpty(result.get(0).getErrors()));
        assertTrue(result.get(0).getErrors().contains(ErrorMessages.EMPTY_TRADES));

    }
}